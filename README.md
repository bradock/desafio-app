<h1>Desafio MBA Mobi</h1>
<h2>Francisco Hugo César (FHC)</h2>

<b>O que consiste o desafio?</b>

Implementar um CRUD no contexto de: 

“Uma pessoa está associada a vários aplicativos por meio de um perfil de acesso.”
“Pessoa deve constar, CPF, nome, data de nascimento e RG.”
“Os perfis serão, usuário comum, gestor e administrador.” 
“Aplicativo deve conter nome e id.”

<b>Quais tecnologias usadas ?</b>

Banco: WorkBench
Backend/service: PHP Laravel
Frontend: Ionic

<b>O que o aplicativo faz?</b>

Na aba de pessoas, conseguimos adicionar uma nova pessoa a partir de um perfil (usuário comum, gestor e administrador), editar e excluir.

Na aba de perfis, conseguimos apenas visualizá-los, pois o mesmo já é regra de negócio da aplicação.

Na aba de aplicativo, conseguimos adicionar um aplicativo de acordo com o perfil de visualização (usuário comum, gestor e administrador), editar e excluir.

Na aba de consulta, conseguimos visualizar todos os aplicativos que tal pessoa tenha acesso.

<b>Como está dividido a pasta do projeto?</b>

O backend em PHP encontra-se no diretório : backend

O frontend em Ionic encontra-se no diretório : frontend

O modelo MER e um popula SQL encontra-se no diretório : Modelo MER

Recursos de vídeo e prints encontra-se no diretório : Recursos

<b>Passo a Passo para execução do projeto:</b>

Abra o modelo MER e execute um “Synchronize model” para a construção do Schema e das tabelas no seu LocalHost, de acordo com a modelagem.

Abra o popular.sql no diretório “Modelo MER” e execute principalmente as Querys de Insert na tabela Perfis (usuário comum, gestor e administrador).


Entre no diretório Backend e execute um COMPOSER install para baixar as dependências necessário para executar o Laravel.

Caso apareça WARNINGS relacionado ao “mbstring” ou “openssl”, abra o arquivo php.ini (encontra-se no diretório onde vc instalou o PHP em sua máquina) e descomente os mesmos. 

Abra o diretório na pasta Backend e procure por um arquivo chamado “.env”, caso não tenha, crie um usando o ".env_exemple" como exemplo e troque as configurações de (DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD) de acordo com seu banco criado na Etapa 1.


Execute o comando na pasta Backend: “php artisan serve” para rodar a API/SERVICE.


Abra o diretório do Frontend e execute o NPM install para baixar as dependências e logo em seguida execute um “ionic serve” na raiz.


<b>ENDPOINTS DA API</b>

<b>PESSOAS:</b>

URL: http://localhost:8000/api/pessoas 
Descrição: Retornar todos as Pessoas do sistema (GET)

URL: http://localhost:8000/api/pessoas/{id}
Descrição: Retornar apenas uma  Pessoa por ID (GET)

URL: http://localhost:8000/api/pessoas 
Body: cpf_pessoa, nome_pessoa,data_nascimento_pessoa, rg_pessoa,perfis_id_perfil.
Descrição: Cadastrar Pessoas do sistema (POST)

URL: http://localhost:8000/api/pessoas/{id}
Body: cpf_pessoa, nome_pessoa,data_nascimento_pessoa, rg_pessoa,perfis_id_perfil.
Descrição: Edita pessoa no sistema a partir do ID (PATCH)

URL: http://localhost:8000/api/pessoas/{id}
Descrição: Deleta  Pessoas do sistema (DELETE)

<b>APLICATIVOS:</b>

URL: http://localhost:8000/api/aplicativos 
Descrição: Retornar todos os aplicativos do sistema (GET)

URL: http://localhost:8000/api/aplicativos/{id}
Descrição: Retornar apenas um aplicativo por ID (GET)

URL: http://localhost:8000/api/aplicativos  
Body: nome_aplicativo.
Descrição: Cadastrar aplicativo no sistema (POST)


URL: http://localhost:8000/api/aplicativos /{id}
Body: nome_aplicativo.
Descrição: Edita aplicativo no sistema (PATCH)

URL: http://localhost:8000/api/aplicativos/{id}
Descrição: Deleta  aplicativo do sistema (DELETE)


<b>PERFIS:</b>

URL: http://localhost:8000/api/perfis  
Descrição: Retornar todos os perfis do sistema (GET)

URL: http://localhost:8000/api/perfis/{id}
Descrição: Retornar apenas um perfis por ID (GET)

URL: http://localhost:8000/api/perfis  
Body: nome_perfil
Descrição: Cadastrar perfis no sistema (POST)


URL: http://localhost:8000/api/perfis/{id}
Body: nome_perfil.
Descrição: Edita perfis no sistema (PATCH)

URL: http://localhost:8000/api/perfis/{id}
Descrição: Deleta  perfis do sistema (DELETE)


<b>APLICATIVO HAS PERFIL:</b>

URL: http://localhost:8000/api/aplicativoHasPerfil 
Descrição: Retornar todos os merges entre aplicativo e perfil, ou seja, todas as ligações entre eles. (GET)

URL: http://localhost:8000/api/aplicativoHasPerfil /{id}
Descrição: Retornar apenas um merge entre aplicativo e perfil, ou seja, todas as ligações entre eles. (GET)

URL: http://localhost:8000/api/aplicativoHasPerfil   
Body: aplicativos_id_aplicativo, perfis_id_perfil.
Descrição: Conecta o aplicativo a um perfil escolhido (POST)



URL: http://localhost:8000/api/aplicativoHasPerfil /{id}
Body: aplicativos_id_aplicativo, perfis_id_perfil..
Descrição: Edita um aplicativo a um perfil escolhido (PATCH)

URL: http://localhost:8000/api/aplicativoHasPerfil/{id}
Descrição: Deleta a classificação do aplicativo de acordo com o perfil (DELETE)

<b>PROCURAR APLICATIVOS ATRAVÉS DE UMA PESSOA:</b>

URL: http://localhost:8000/api/aplicativosProcurar/{id_pessoa} (GET)


<h6>Francisco Hugo César (FHC)</h6>












