<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplicativo extends Model
{

    protected $primaryKey = "id_aplicativo";

    protected $fillable = [
        "nome_aplicativo"
    ];
}
