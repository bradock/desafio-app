<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AplicativosHasPerfi extends Model
{
    protected $primaryKey = "aplicativos_id_aplicativo, perfis_id_perfil";

    protected $fillable   = [
        "aplicativos_id_aplicativo", 
        "perfis_id_perfil"
    ]; 
}
