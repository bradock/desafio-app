<?php

namespace App\Http\Controllers;

use DB;
use App\AplicativosHasPerfi;
use Illuminate\Http\Request;

class AplicativoHasPerfilController extends Controller
{
    public function index()
    {
        return AplicativosHasPerfi::all();
    }

    public function show($idAplicativo)
    {
        //$app = DB::table('aplicativos_has_perfis')->where('aplicativos_id_aplicativo', '=', $idAplicativo)->get();

        $app = DB::table('aplicativos_has_perfis') 
        ->join('perfis', 'aplicativos_has_perfis.perfis_id_perfil', '=', 'perfis.id_perfil')
        ->where('aplicativos_has_perfis.aplicativos_id_aplicativo', '=', $idAplicativo)
        ->select(['aplicativos_has_perfis.aplicativos_id_aplicativo','perfis.nome_perfil'])
        ->get();

        return $app;
    }

    public function create(Request $request)
    {

        $request->validate([
            "aplicativos_id_aplicativo" => "required",
            "perfis_id_perfil"          => "required"
        ]);

        $app = DB::table('aplicativos_has_perfis')->insert(
            array('aplicativos_id_aplicativo' => $request->input("aplicativos_id_aplicativo"), 'perfis_id_perfil' => $request->input("perfis_id_perfil"))
        );

        return $app;
    }

    public function update($idAplicativo, $idPerfil, Request $request)
    {   

        $app = DB::table('aplicativos_has_perfis')
            ->where('aplicativos_id_aplicativo', '=', $idAplicativo)
            ->Where('perfis_id_perfil', '=', $idPerfil)
            ->update(array('aplicativos_id_aplicativo' => $request->input("aplicativos_id_aplicativo"), 'perfis_id_perfil' => $request->input("perfis_id_perfil")));
            //->get();

        $att = DB::table('aplicativos_has_perfis')
            ->where('aplicativos_id_aplicativo', '=', $request->input("aplicativos_id_aplicativo"))
            ->Where('perfis_id_perfil', '=', $request->input("perfis_id_perfil"))
            ->get();

        return $att;
    }

    public function delete($idAplicativo, $idPerfil)
    {
        $app = DB::table('aplicativos_has_perfis')
        ->where('aplicativos_id_aplicativo', '=', $idAplicativo)
        ->Where('perfis_id_perfil', '=', $idPerfil)
        ->delete();

        $talk = "";

        if($app != 0){
            $talk = "Excluido com sucesso";
        }else{
            $talk = "Error ao excluir";
        }
        
        return $talk;
    }
}
