<?php

namespace App\Http\Controllers;
use DB;
use App\Aplicativo;
use Illuminate\Http\Request;

class AplicativosController extends Controller
{
    public function index()
    {
        return Aplicativo::all();
    }

    public function show(Aplicativo $aplicativo)
    {
        return $aplicativo;
    }

    public function create(Request $request)
    {

        $request->validate([
            "nome_aplicativo" => "required"
        ]);

        $aplicativo = Aplicativo::create([
            "nome_aplicativo" => $request->input('nome_aplicativo')
        ]);

        return $aplicativo;
    }

    public function update(Request $request, Aplicativo $aplicativo)
    {
        if(isset($request["nome_aplicativo"])){
            $aplicativo->nome_aplicativo = $request->input("nome_aplicativo");
        }

        $aplicativo->save();

        return $aplicativo;
    }

    public function delete(Aplicativo $aplicativo)
    {
        $app = DB::table('aplicativos_has_perfis')
        ->where('aplicativos_id_aplicativo', '=', $aplicativo->id_aplicativo)
        ->delete();

         $aplicativo->delete();

         return Response()->json([
            'success' => true
            ], 200); 

        return $aplicativo;
    }

    public function procurarAplicativo($perfil){

        $app = DB::table('pessoas') 
        ->join('perfis', 'pessoas.perfis_id_perfil', '=', 'perfis.id_perfil')
        ->join('aplicativos_has_perfis', 'perfis.id_perfil', '=', 'aplicativos_has_perfis.perfis_id_perfil')
        ->join('aplicativos', 'aplicativos_has_perfis.aplicativos_id_aplicativo', '=', 'aplicativos.id_aplicativo')
        ->where('pessoas.id_pessoa', '=', $perfil)
        ->select('aplicativos.nome_aplicativo')
        ->distinct()
        ->get();

        return $app;

    }
}
