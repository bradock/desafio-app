<?php

namespace App\Http\Controllers;
use DB;
use App\Perfi;
use Illuminate\Http\Request;

class PerfisController extends Controller
{
    public function index()
    {
        return Perfi::all();
    }

    public function show(Perfi $perfil)
    {
        return $perfil;
    }

    public function create(Request $request)
    {

        $request->validate([
            "nome_perfil" => "required"
        ]);


        $perfil = Perfi::create([
            "nome_perfil"  => $request->input("nome_perfil")
        ]);

        return $perfil;
    }

    public function update(Request $request, Perfi $perfil)
    {
        if(isset($request["nome_perfil"])){
            $perfil->nome_perfil = $request->input("nome_perfil");
        }

        $perfil->save();

        return $perfil;

    }

    public function delete(Perfi $perfil)
    {
        $appA = DB::table('aplicativos_has_perfis')
        ->where('perfis_id_perfil', '=', $perfil->id_perfil)
        ->delete();

        $appP = DB::table('pessoas')
        ->where('perfis_id_perfil', '=', $perfil->id_perfil)
        ->delete();

        $perfil->delete();

         return Response()->json([
            'success' => true
        ], 200); 


    }
}
