<?php

namespace App\Http\Controllers;

use App\Pessoa;
use DB;
use Illuminate\Http\Request;


class PessoasController extends Controller
{
    
    public function index()
    {
        return Pessoa::all();
    }

    public function show(Pessoa $pessoa)
    {

        //$user = DB::table('pessoas')->where('id_pessoa', '=', 1)->get();
        return $pessoa;
    }

    public function create(Request $request)
    {

        $request->validate([
            "perfis_id_perfil"       => "required",
            "cpf_pessoa"             => "required|max:11",
            "nome_pessoa"            => "required|max:60",
            "data_nascimento_pessoa" => "required|date",
            "rg_pessoa"              => "required|max:15"
        ]);

        $pessoa = Pessoa::create([
            "perfis_id_perfil"       => $request->input("perfis_id_perfil"),
            "cpf_pessoa"             => $request->input("cpf_pessoa"),
            "nome_pessoa"            => $request->input("nome_pessoa"),
            "data_nascimento_pessoa" => $request->input("data_nascimento_pessoa"),
            "rg_pessoa"              => $request->input("rg_pessoa"),
        ]);


        return $pessoa;
    }

    public function update(Request $request, Pessoa $pessoa)
    {

        if(isset($request["nome_pessoa"])){
            $pessoa->nome_pessoa = $request->input('nome_pessoa');
        }

        if(isset($request["rg_pessoa"])){
            $pessoa->rg_pessoa = $request->input('rg_pessoa');
        }

        if(isset($request["data_nascimento_pessoa"])){
            $pessoa->data_nascimento_pessoa = $request->input('data_nascimento_pessoa');
        }

        if(isset($request["perfis_id_perfil"])){
            $pessoa->perfis_id_perfil = $request->input('perfis_id_perfil');
        }

        if(isset($request["cpf_pessoa"])){
            $pessoa->cpf_pessoa = $request->input('cpf_pessoa');
        }

        $pessoa->save();

        return $pessoa;
    }

    public function delete(Pessoa $pessoa)
    {
        $pessoa->delete();

        return response()->json(['success' => true], 200);
    }
    

}
