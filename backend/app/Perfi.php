<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfi extends Model
{
    protected $primaryKey = "id_perfil";

    protected $fillable = [
        "nome_perfil"
    ];
}
