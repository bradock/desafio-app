<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $primaryKey = 'id_pessoa';

    protected $fillable = [
        "perfis_id_perfil",
        "cpf_pessoa",
        "nome_pessoa",
        "data_nascimento_pessoa",
        "rg_pessoa"
    ];
}
