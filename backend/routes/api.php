<?php

use Illuminate\Http\Request;

// PESSOAS

Route::get("pessoas", "PessoasController@index");

Route::get("pessoas/{pessoa}",  "PessoasController@show");

Route::post("pessoas", "PessoasController@create");

Route::patch("pessoas/{pessoa}", "PessoasController@update");

Route::delete("pessoas/{pessoa}", "PessoasController@delete");

// APLICATIVOS

Route::get("aplicativos", "AplicativosController@index");

Route::get("aplicativos/{aplicativo}","AplicativosController@show");

Route::post("aplicativos", "AplicativosController@create");

Route::patch("aplicativos/{aplicativo}", "AplicativosController@update");

Route::delete("aplicativos/{aplicativo}", "AplicativosController@delete");

Route::get("aplicativosProcurar/{perfil}", "AplicativosController@procurarAplicativo");

// APLICATIVO HAS PERFIL

Route::get("aplicativoHasPerfil", "AplicativoHasPerfilController@index");

Route::get("aplicativoHasPerfil/{aplicativoHasPerfil}", "AplicativoHasPerfilController@show");

Route::post("aplicativoHasPerfil", "AplicativoHasPerfilController@create");

Route::patch("aplicativoHasPerfil/{aplicativoHasPerfil}/{idPerfil}", "AplicativoHasPerfilController@update");

Route::delete("aplicativoHasPerfil/{aplicativoHasPerfil}/{idPerfil}", "AplicativoHasPerfilController@delete");

// PERFIS

Route::get("perfis", "PerfisController@index");

Route::get("perfis/{perfil}", "PerfisController@show");

Route::post("perfis", "PerfisController@create");

Route::patch("perfis/{perfil}", "PerfisController@update");

Route::delete("perfis/{perfil}", "PerfisController@delete");





