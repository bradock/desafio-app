Essa etapa consiste na implementação de um caso de uso, no qual avaliaremos seus conhecimentos técnicos.

Pedimos que implemente uma aplicação de exemplo baseado nas especificações abaixo e submeta o mais breve possível. A entrega deverá ser feita através de um projeto hospedado no github/gitlab contendo a explicação no README.md de como subir os serviços e realizar build do projeto implementado.

O tempo será cronometrado a partir do envio deste e-mail até o momento da sua resposta.

Deve ser implementado um CRUD com todas as operações relacionadas e persistida em banco de dados. Deve atender o seguinte cenário.

Uma pessoa está associada a vários aplicativos por meio de um perfil de acesso.
Pessoa deve conter, CPF, nome, data de nascimento e RG.
Os perfis serão, usuário comum, gestor e administrador.
Aplicativo deve conter nome e id.
Atenção para os relacionamentos entre as entidades.

Será necessário implementar o backend em PHP ou Java, envie também o MER.

Requisitos técnicos:

Backend Java: Spring boot 2.x;
Backend PHP: Laravel 5.x ou Lumen 5.x;
Mobile: Ionic 5;
Banco de dados: Postgres ou MySQL


Caso tenha alguma dúvida, por favor, entre em contato.