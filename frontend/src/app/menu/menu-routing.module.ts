import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: "pessoa",
        loadChildren: () => import('../pages/pessoa/pessoa.module').then(m => m.PessoaPageModule)
      },
      {
        path: 'add-pessoa',
        loadChildren: () => import('../pages/add-pessoa/add-pessoa.module').then( m => m.AddPessoaPageModule)
      },
      {
        path: 'edit-pessoa/:id',
        loadChildren: () => import('../pages/edit-pessoa/edit-pessoa.module').then( m => m.EditPessoaPageModule)
      },
      {
        path: 'perfil',
        loadChildren: () => import('../pages/perfil/perfil.module').then( m => m.PerfilPageModule)
      },
      {
        path: 'aplicativo',
        loadChildren: () => import('../pages/aplicativo/aplicativo.module').then( m => m.AplicativoPageModule)
      },
      {
        path: 'add-aplicativo',
        loadChildren: () => import('../pages/add-aplicativo/add-aplicativo.module').then( m => m.AddAplicativoPageModule)
      },
      {
        path: 'edit-aplicativo/:id',
        loadChildren: () => import('../pages/edit-aplicativo/edit-aplicativo.module').then( m => m.EditAplicativoPageModule)
      },
      {
        path: 'consulta',
        loadChildren: () => import('../pages/consulta/consulta.module').then( m => m.ConsultaPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/menu/pessoa',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/menu/pessoa',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
