import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAplicativoPage } from './add-aplicativo.page';

const routes: Routes = [
  {
    path: '',
    component: AddAplicativoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddAplicativoPageRoutingModule {}
