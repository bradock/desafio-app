import { ModuloModule } from './../../components/modulo.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddAplicativoPageRoutingModule } from './add-aplicativo-routing.module';

import { AddAplicativoPage } from './add-aplicativo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddAplicativoPageRoutingModule,
    ModuloModule
  ],
  declarations: [AddAplicativoPage]
})
export class AddAplicativoPageModule {}
