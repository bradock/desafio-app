import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddAplicativoPage } from './add-aplicativo.page';

describe('AddAplicativoPage', () => {
  let component: AddAplicativoPage;
  let fixture: ComponentFixture<AddAplicativoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAplicativoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddAplicativoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
