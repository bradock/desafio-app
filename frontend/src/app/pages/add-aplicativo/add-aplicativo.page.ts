import { AppHasModel } from './../../services/appHasPerfil.model';
import { PerfilService } from './../../services/perfil.service';
import { AplicativoService } from './../../services/aplicativo.service';
import { Component, OnInit } from '@angular/core';
import { Aplicativo } from 'src/app/services/aplicativo.model';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-add-aplicativo',
  templateUrl: './add-aplicativo.page.html',
  styleUrls: ['./add-aplicativo.page.scss'],
})
export class AddAplicativoPage implements OnInit {

  constructor(private router: Router, private toastController : ToastController, private service : AplicativoService, private servicePerfis : PerfilService) { }

  aplicativo : Aplicativo = {
    created_at: null,
    updated_at: null,
    nome_aplicativo: "",
  };

  perfilSelecionado = "2";

  perfis = [];

  appHasPerfil : AppHasModel = {
    aplicativos_id_aplicativo: this.aplicativo.id_aplicativo,
    perfis_id_perfil: this.perfilSelecionado,
    created_at: null,
    updated_at: null
  };

  ngOnInit() {
    this.servicePerfis.showTipo().subscribe(obj => {
      this.perfis = obj;
      //console.log(this.perfis);
    });
  }


  addAplicativo(){

    console.log(this.appHasPerfil.perfis_id_perfil);


    if(this.aplicativo.nome_aplicativo.length == 0 ){
      this.mensagemCadastro(true);
    }

    this.service.create(this.aplicativo).subscribe( e => {
      console.log("oooooooooooo");
      this.mensagemCadastro(false);
      this.appHasPerfil.aplicativos_id_aplicativo = e.id_aplicativo;

      this.service.appHasPerfil(this.appHasPerfil).subscribe( () => {
        console.log("Perfil Vinculado ao app");
      });

    });

    

  }

  async mensagemCadastro(teveError: boolean ) {
    const toast = await this.toastController.create({
      message: teveError ? "Preencha todos os campos" : 'Criado com Sucesso',
      position: 'bottom',
      duration: 2000,
      color: teveError ? "danger" :"primary"
    });
    toast.present();

    if(!teveError){
      //this.router.navigateByUrl("/tabs/menu/aplicativo");
      this.router.navigate(['/tabs/menu/aplicativo'])
    }
  }

}
