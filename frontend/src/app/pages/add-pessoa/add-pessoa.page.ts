import { PerfilService } from './../../services/perfil.service';
import { PessoaService } from './../../services/pessoa.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Pessoa } from 'src/app/services/pessoa.model';

@Component({
  selector: 'app-add-pessoa',
  templateUrl: './add-pessoa.page.html',
  styleUrls: ['./add-pessoa.page.scss'],
})
export class AddPessoaPage implements OnInit {

  constructor(private toastController: ToastController, private router: Router, private service : PessoaService, private servicePerfis : PerfilService) { }


  pessoa: Pessoa = {
    nome_pessoa             : "",
    rg_pessoa               : "",
    cpf_pessoa              : "",
    data_nascimento_pessoa  : "2020-02-02",
    perfis_id_perfil        : "2",
    created_at              : null,
    updated_at              : null
  };

  perfis = [];

  loadin = false;

  ngOnInit() {
    this.servicePerfis.showTipo().subscribe(obj => {
      this.perfis = obj;
      //console.log(this.perfis);
    });
  }


  addPessoas(){

    console.log(this.pessoa.nome_pessoa.length);

    if(this.pessoa.nome_pessoa.length == 0 || this.pessoa.rg_pessoa.length == 0 || this.pessoa.cpf_pessoa.length == 0  ){
      this.mensagemCadastro(true);
    }

    this.pessoa.data_nascimento_pessoa = this.pessoa.data_nascimento_pessoa.split("T",3)[0];
    this.service.create(this.pessoa).subscribe( () => {
      this.mensagemCadastro(false);
    });
    
  }

  async mensagemCadastro(teveError: boolean ) {
    const toast = await this.toastController.create({
      message: teveError ? "Preencha todos os campos" : 'Criado com Sucesso',
      position: 'bottom',
      duration: 2000,
      color: teveError ? "danger" :"primary"
    });
    toast.present();

    if(!teveError){
      this.router.navigateByUrl("/tabs/menu/pessoa");
    }
  }

}
