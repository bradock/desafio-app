import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AplicativoPage } from './aplicativo.page';

describe('AplicativoPage', () => {
  let component: AplicativoPage;
  let fixture: ComponentFixture<AplicativoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicativoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AplicativoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
