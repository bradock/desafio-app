import { AplicativoService } from './../../services/aplicativo.service';
import { Aplicativo } from './../../services/aplicativo.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aplicativo',
  templateUrl: './aplicativo.page.html',
  styleUrls: ['./aplicativo.page.scss'],
})
export class AplicativoPage implements OnInit {

  constructor(private service : AplicativoService) { }

  loading : boolean = true;

  aplicativos : Aplicativo[];

  ngOnInit(): void {

    this.loading = false;

    this.service.read().subscribe( obj => {
      this.aplicativos = obj;
    });

  }


  doRefresh(event){
    setTimeout(() => {
      console.log('Async operation has ended');
      this.ngOnInit();
      event.target.complete();
    }, 2000);

  }

  ionViewWillEnter(){
    this.loading = false;

    this.service.read().subscribe( obj => {
      this.aplicativos = obj;
    });
  }

  ionViewDidLeave(){
    this.loading = false;

    this.service.read().subscribe( obj => {
      this.aplicativos = obj;
    });
  }



}
