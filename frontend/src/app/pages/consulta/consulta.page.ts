import { PessoaService } from './../../services/pessoa.service';
import { PerfilService } from './../../services/perfil.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.page.html',
  styleUrls: ['./consulta.page.scss'],
})
export class ConsultaPage implements OnInit {

  constructor(private servicePerfis : PerfilService, private servicePessoas : PessoaService) { }

  acesso : boolean = false;

  pessoas = [];

  tt = "1";

  aplicativos = [];

  ngOnInit() {
    this.servicePessoas.read().subscribe( obj => {
      this.pessoas = obj;
    });

  }

  procurar(e){
    this.acesso = true;
    this.servicePerfis.procurar(e).subscribe(obj => {
      this.aplicativos = obj;
    });
  }

}
