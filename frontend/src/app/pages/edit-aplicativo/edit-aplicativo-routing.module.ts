import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditAplicativoPage } from './edit-aplicativo.page';

const routes: Routes = [
  {
    path: '',
    component: EditAplicativoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditAplicativoPageRoutingModule {}
