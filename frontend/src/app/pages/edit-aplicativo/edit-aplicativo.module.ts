import { ModuloModule } from './../../components/modulo.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditAplicativoPageRoutingModule } from './edit-aplicativo-routing.module';

import { EditAplicativoPage } from './edit-aplicativo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditAplicativoPageRoutingModule,
    ModuloModule
  ],
  declarations: [EditAplicativoPage]
})
export class EditAplicativoPageModule {}
