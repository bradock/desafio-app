import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditAplicativoPage } from './edit-aplicativo.page';

describe('EditAplicativoPage', () => {
  let component: EditAplicativoPage;
  let fixture: ComponentFixture<EditAplicativoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAplicativoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditAplicativoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
