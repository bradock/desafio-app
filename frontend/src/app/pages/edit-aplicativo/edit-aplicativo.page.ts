import { AplicativoService } from './../../services/aplicativo.service';
import { Component, OnInit } from '@angular/core';
import { Aplicativo } from 'src/app/services/aplicativo.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AppHasModel } from 'src/app/services/appHasPerfil.model';

@Component({
  selector: 'app-edit-aplicativo',
  templateUrl: './edit-aplicativo.page.html',
  styleUrls: ['./edit-aplicativo.page.scss'],
})
export class EditAplicativoPage implements OnInit {

  constructor(private service : AplicativoService,private route: ActivatedRoute, private router: Router, private toastController : ToastController) {

   }

  aplicativo : Aplicativo = {
    created_at: null,
    updated_at: null,
    nome_aplicativo: "",
  };

  perfilSelecionado = "2";

  appHasPerfil : AppHasModel = {
    aplicativos_id_aplicativo: this.aplicativo.id_aplicativo,
    perfis_id_perfil: this.perfilSelecionado,
    created_at: null,
    updated_at: null,
    nome_perfil: "",
  };

  ngOnInit() {
    this.route.params.subscribe( parametros => {
      this.aplicativo.id_aplicativo = parametros['id'];

      if (parametros['id'] != undefined) {
        this.aplicativo.id_aplicativo = parametros['id'] + "";
        //console.log(this.id);
      }else{
        this.router.navigateByUrl('/tabs/menu/pessoa');
      }

    });

    this.service.readById(this.aplicativo.id_aplicativo).subscribe( obj => {
      this.aplicativo.nome_aplicativo        = obj.nome_aplicativo;
      //console.log(this.pessoa);
    });

    this.service.showPerfilByApp(this.aplicativo.id_aplicativo).subscribe( obj => {
      this.appHasPerfil.aplicativos_id_aplicativo = obj[0].aplicativos_id_aplicativo;
      this.appHasPerfil.nome_perfil               = obj[0].nome_perfil;
      console.log(obj);
    });
  }

  EditPessoas(){

    if(this.aplicativo.nome_aplicativo.length  == 0 ){
      this.mensagemCadastro(true, "Error ao editar");
    }

    this.service.update(this.aplicativo).subscribe( () => {
      this.mensagemCadastro(false, "Editado com Sucesso");
    });
  }

  DeletePessoas(){

    this.service.delete(this.aplicativo.id_aplicativo).subscribe( () => {
      this.mensagemCadastro(false, "Excluido com sucesso");
    });

  }

  async mensagemCadastro(teveError: boolean, mensagem : string ) {
    const toast = await this.toastController.create({
      message: mensagem,
      position: 'bottom',
      duration: 2000,
      color: teveError ? "danger" :"primary"
    });
    toast.present();

    this.router.navigateByUrl("/tabs/menu/aplicativo");

    //this.router.navigateByUrl('/mensagem'); 
  }

}
