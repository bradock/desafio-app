import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPessoaPage } from './edit-pessoa.page';

const routes: Routes = [
  {
    path: '',
    component: EditPessoaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPessoaPageRoutingModule {}
