import { ModuloModule } from './../../components/modulo.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPessoaPageRoutingModule } from './edit-pessoa-routing.module';

import { EditPessoaPage } from './edit-pessoa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPessoaPageRoutingModule,
    ModuloModule
  ],
  declarations: [EditPessoaPage]
})
export class EditPessoaPageModule {}
