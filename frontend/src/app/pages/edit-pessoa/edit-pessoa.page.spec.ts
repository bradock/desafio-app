import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditPessoaPage } from './edit-pessoa.page';

describe('EditPessoaPage', () => {
  let component: EditPessoaPage;
  let fixture: ComponentFixture<EditPessoaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPessoaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditPessoaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
