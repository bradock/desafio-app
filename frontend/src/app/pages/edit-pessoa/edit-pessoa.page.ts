import { PerfilService } from './../../services/perfil.service';
import { PessoaService } from './../../services/pessoa.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Pessoa } from 'src/app/services/pessoa.model';

@Component({
  selector: 'app-edit-pessoa',
  templateUrl: './edit-pessoa.page.html',
  styleUrls: ['./edit-pessoa.page.scss'],
})
export class EditPessoaPage implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private toastController : ToastController, private servicePessoa : PessoaService, private servicePerfis : PerfilService) { }

  pessoa: Pessoa = {
    id_pessoa               : "1",
    nome_pessoa             : "",
    rg_pessoa               : "",
    cpf_pessoa              : "",
    data_nascimento_pessoa  : "2020-02-02",
    perfis_id_perfil        : "2",
    created_at              : null,
    updated_at              : null
  };

  perfis = [];

  ngOnInit() {
    this.route.params.subscribe( parametros => {
      this.pessoa.id_pessoa = parametros['id'];

      if (parametros['id'] != undefined) {
        this.pessoa.id_pessoa = parametros['id'];
        //console.log(this.id);
      }else{
        this.router.navigateByUrl('/tabs/menu/pessoa');
      }

    });

    this.servicePessoa.readById(this.pessoa.id_pessoa).subscribe( obj => {
      this.pessoa.nome_pessoa            = obj.nome_pessoa;
      this.pessoa.rg_pessoa              = obj.rg_pessoa;
      this.pessoa.cpf_pessoa             = obj.cpf_pessoa;
      this.pessoa.data_nascimento_pessoa = obj.data_nascimento_pessoa;
      this.pessoa.perfis_id_perfil       = obj.perfis_id_perfil + "";
      //console.log(this.pessoa);
    });

    this.servicePerfis.showTipo().subscribe(obj => {
      this.perfis = obj;
      //console.log(this.perfis);
    });
  }

  EditPessoas(){

    if(this.pessoa.nome_pessoa.length == 0 || this.pessoa.rg_pessoa.length == 0 || this.pessoa.cpf_pessoa.length == 0  ){
      this.mensagemCadastro(true, "Error ao editar");
    }

    this.pessoa.data_nascimento_pessoa = this.pessoa.data_nascimento_pessoa.split("T",3)[0];
    this.servicePessoa.update(this.pessoa).subscribe( () => {
      this.mensagemCadastro(false, "Editado com Sucesso");
    });
  }

  DeletePessoas(){

    this.servicePessoa.delete(this.pessoa.id_pessoa).subscribe( () => {
      this.mensagemCadastro(false, "Excluido com sucesso");
    });

  }

  async mensagemCadastro(teveError: boolean, mensagem : string ) {
    const toast = await this.toastController.create({
      message: mensagem,
      position: 'bottom',
      duration: 2000,
      color: teveError ? "danger" :"primary"
    });
    toast.present();

    this.router.navigateByUrl("/tabs/menu/pessoa");

    //this.router.navigateByUrl('/mensagem'); 
  }
 

}
