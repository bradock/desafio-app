import { PerfilService } from './../../services/perfil.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  constructor(private servicePerfis: PerfilService) { }

  perfis = [];

  loading : boolean = true;

  ngOnInit() {
    this.servicePerfis.showTipo().subscribe(obj => {
      this.perfis = obj;
      //console.log(this.perfis);
      this.loading = false;

    });

  }

  ionViewDidLeave(){
    this.servicePerfis.showTipo().subscribe(obj => {
      this.perfis = obj;
      //console.log(this.perfis);
      this.loading = false;

    });
  }

}
