import { PessoaService } from './../../services/pessoa.service';
import { Component, OnInit } from '@angular/core';
import { Pessoa } from 'src/app/services/pessoa.model';
import { timeInterval } from 'rxjs/operators';

@Component({
  selector: 'app-pessoa',
  templateUrl: './pessoa.page.html',
  styleUrls: ['./pessoa.page.scss'],
})
export class PessoaPage implements OnInit {

  constructor(private service: PessoaService) { }

  pessoas: Pessoa[];

  loading : boolean = true;

  countGeral : number;


  ngOnInit(): void {
    this.loading = false;

    this.service.read().subscribe( result => {
      this.pessoas = result;
      console.log(this.pessoas);
    });
  }

  doRefresh(event){
    setTimeout(() => {
      console.log('Async operation has ended');
      this.ngOnInit();
      event.target.complete();
    }, 2000);

  }

  ionViewWillEnter(){
    this.service.read().subscribe( result => {
      this.pessoas = result;
      this.loading = false;
      //console.log(this.arrayPessoas);
    });
  }


  ionViewDidLeave(){
    this.service.read().subscribe( result => {
      this.pessoas = result;
      this.loading = false;
      //console.log(this.arrayPessoas);
    });
  }

}
