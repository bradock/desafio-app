export interface Aplicativo{
    id_aplicativo     ?:string,
    nome_aplicativo   : string,
    created_at        : string,
    updated_at        : string
}