import { AppHasModel } from './appHasPerfil.model';
import { Aplicativo } from './aplicativo.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, EMPTY } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AplicativoService {

  constructor(private http: HttpClient, private toastController : ToastController) { }

  baseUrl = "http://localhost:8000/api/aplicativos"

  uriBaseAppHasPerfil = "http://localhost:8000/api/aplicativoHasPerfil/";

  create(product: Aplicativo): Observable<Aplicativo>{
    return this.http.post<Aplicativo>(this.baseUrl, product).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  async showMenssage(mensagem : string){
    const toast = await this.toastController.create({
      message: mensagem,
      position: 'bottom',
      duration: 4000,
      color: "danger"
    });
    toast.present();
  }

  errorHandler(e: any): Observable<any>{
    this.showMenssage("Error ao acessar o servidor");
    return EMPTY;
  }

  read(): Observable<Aplicativo[]>{
    return this.http.get<Aplicativo[]>(this.baseUrl).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  readById(id: string): Observable<Aplicativo>{
    const url = `${this.baseUrl}/${id}`
    return this.http.get<Aplicativo>(url).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  update(product: Aplicativo): Observable<Aplicativo>{
    const url = `${this.baseUrl}/${product.id_aplicativo}`
    return this.http.patch<Aplicativo>(url,product).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  delete(id: string): Observable<Aplicativo>{
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<Aplicativo>(url).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  appHasPerfil(app : AppHasModel): Observable<any>{
    return this.http.post<any>(this.uriBaseAppHasPerfil, app).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )

  }


  showPerfilByApp(id: string): Observable<AppHasModel>{
    let uri = this.uriBaseAppHasPerfil + id;
    return this.http.get(uri).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }
  
}
