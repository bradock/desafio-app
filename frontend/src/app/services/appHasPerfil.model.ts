export interface AppHasModel{
    aplicativos_id_aplicativo:                     string,
    perfis_id_perfil:                              string,
    updated_at:                                    string,
    created_at:                                    string,
    nome_perfil?:                                  string
}