export interface Perfil{
    id                      ?:  number,
    nome_pessoa             : string,
    rg_pessoa               : string,
    cpf_pessoa              : string,
    data_nascimento_pessoa  : string,
    perfis_id_perfil        : string
}