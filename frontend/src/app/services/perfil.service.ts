import { catchError, map } from 'rxjs/operators';
import { Observable, EMPTY } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  constructor(private http : HttpClient, private toastController : ToastController) {}

  urlBase : string = "http://localhost:8000/api/perfis";

  urlProcura : string = "http://localhost:8000/api/aplicativosProcurar/";

  showTipo() : Observable<any[]>{
    return this.http.get<any[]>(this.urlBase).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    );
  }

  procurar(e : number): Observable<any[]>{
    let uri = this.urlProcura + e;
    return this.http.get(uri).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    );
  }

  async showMenssage(mensagem : string){
    const toast = await this.toastController.create({
      message: mensagem,
      position: 'bottom',
      duration: 4000,
      color: "danger"
    });
    toast.present();
  }

  errorHandler(e: any): Observable<any>{
    this.showMenssage("Error ao acessar o servidor");
    return EMPTY;
  }

}
