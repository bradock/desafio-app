import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import { ToastController } from '@ionic/angular';
import { Pessoa } from './pessoa.model';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  constructor(private http : HttpClient , private toastController: ToastController) { }

  baseUrl : string = "http://localhost:8000/api/pessoas";


  async showMenssage(mensagem : string){
    const toast = await this.toastController.create({
      message: mensagem,
      position: 'bottom',
      duration: 4000,
      color: "danger"
    });
    toast.present();
  }

  errorHandler(e: any): Observable<any>{
    
    this.showMenssage("Error ao acessar o servidor");
    return EMPTY;
  }

  read() : Observable<Pessoa[]>{
    return this.http.get<Pessoa[]>(this.baseUrl).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  create(pessoa : Pessoa): Observable<Pessoa>{

    return this.http.post<Pessoa>(this.baseUrl, pessoa).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    );
  }

  readById(id: string): Observable<Pessoa>{

    const url = `${this.baseUrl}/${id}`
    return this.http.get<Pessoa>(url).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  update(pessoa: Pessoa): Observable<Pessoa>{
    const url = `${this.baseUrl}/${pessoa.id_pessoa}`
    return this.http.patch<Pessoa>(url,pessoa).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }

  delete(id: string): Observable<Pessoa>{
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<Pessoa>(url).pipe(
      map(obj => obj),
      catchError(e => this.errorHandler(e))
    )
  }


}
