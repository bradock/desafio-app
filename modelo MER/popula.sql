# Auth Francisco Hugo César 

# Perfis
SELECT * FROM perfis;

INSERT INTO perfis(nome_perfil)
	VALUES("Usuário comum");
    
INSERT INTO perfis(nome_perfil)
	VALUES("Gestor");
    
INSERT INTO perfis(nome_perfil)
	VALUES("Administrador");

# Pessoas
SELECT * FROM pessoas;

# Pessoa com perfil de Administrador 
INSERT INTO pessoas
	(perfis_id_perfil, cpf_pessoa, nome_pessoa, data_nascimento_pessoa, rg_pessoa)
VALUES(6, "06548975114","Francisco Hug33o","1998-03-28","22058948");

# Pessoa com perfil de Administrador 
INSERT INTO pessoas
	(perfis_id_perfil, cpf_pessoa, nome_pessoa, data_nascimento_pessoa, rg_pessoa)
VALUES(7, "02550256884","Marlon","1964-11-12","12345678");

# Pessoa com perfil de Gestor
INSERT INTO pessoas
	(perfis_id_perfil, cpf_pessoa, nome_pessoa, data_nascimento_pessoa, rg_pessoa)
VALUES(7, "02584258415","Lucas Pereira","1991-02-02","00025889");

# Pessoa com perfil de Usuário Comum
INSERT INTO pessoas
	(perfis_id_perfil, cpf_pessoa, nome_pessoa, data_nascimento_pessoa, rg_pessoa)
VALUES(8, "12345678910","Cleiton","1980-06-16","12389788");

# Aplicativos
SELECT * FROM aplicativos;

INSERT INTO aplicativos(nome_aplicativo)
	VALUES ("Aplicativo Matrix");
    
INSERT INTO aplicativos(nome_aplicativo)
	VALUES ("Aplicativo ENEM");
    
INSERT INTO aplicativos(nome_aplicativo)
	VALUES ("Aplicativo SISU");
    
INSERT INTO aplicativos(nome_aplicativo)
	VALUES ("Aplicativo ANA");
    
# Aplicativo_has_aplicativos

SELECT * FROM aplicativos_has_perfis;

# Cadastrar um aplicativo de acordo com o perfil de acesso
INSERT INTO aplicativos_has_perfis(aplicativos_id_aplicativo, perfis_id_perfil)
	VALUES(57,8);
    
# Cadastrar um aplicativo de acordo com o perfil de acesso
INSERT INTO aplicativos_has_perfis(aplicativos_id_aplicativo, perfis_id_perfil)
	VALUES(58,7);
    
# Cadastrar um aplicativo de acordo com o perfil de acesso
INSERT INTO aplicativos_has_perfis(aplicativos_id_aplicativo, perfis_id_perfil)
	VALUES(59,6);
    
# Cadastrar um aplicativo de acordo com o perfil de acesso
INSERT INTO aplicativos_has_perfis(aplicativos_id_aplicativo, perfis_id_perfil)
	VALUES(60,6);
    
commit;

## =============================== Consultas

# Acessos de pessoas aos aplicativos de acordo com seu perfil
SELECT distinct(aplicativos.nome_aplicativo)
	FROM pessoas INNER JOIN perfis on pessoas.perfis_id_perfil = perfis.id_perfil 
    INNER JOIN aplicativos_has_perfis on perfis.id_perfil = aplicativos_has_perfis.perfis_id_perfil 
    INNER JOIN aplicativos ON aplicativos_has_perfis.aplicativos_id_aplicativo = aplicativos.id_aplicativo where pessoas.id_pessoa= 50;